export default {
    MAX_ATTACHMENT_SIZE: 5000000,
    STRIPE_KEY: "pk_test_RNm1ozNi3ukURG99Ioca8vqX00aRmD0nSg",
    s3: {
      REGION: "us-east-2",
      BUCKET: "notes-app-api-prod-serverlessdeploymentbucket-1d5ok4xev75y7"
    },
    apiGateway: {
      REGION: "us-east-2",
      URL: "https://k8fdiek980.execute-api.us-east-2.amazonaws.com/prod"
    },
    cognito: {
      REGION: "us-east-2",
      USER_POOL_ID: "us-east-2_uul3qtJ4C",
      APP_CLIENT_ID: "4k3kgbg0ltlc3p1pnek3gcduj8",
      IDENTITY_POOL_ID: "us-east-2:1b37aca0-619a-4add-8f6f-c73e0c9b1bed"
    }
  };